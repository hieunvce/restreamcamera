ARG RASPBIAN_VERSION=stretch
FROM resin/rpi-raspbian:${RASPBIAN_VERSION}

ARG RASPBIAN_VERSION

# -----Install OpenCV3-------
RUN cd /tmp \
    apt-get update \
    && apt-get install -y --no-install-recommends apt-utils \
    # install necessary build tools \
    && apt-get -qy install build-essential \
    git cmake cmake-curses-gui \
    pkg-config zip unzip wget \
    && apt-get -qy install libjpeg-dev \
	libtiff5-dev \
	libjasper-dev \
	libpng-dev \
	libeigen3-dev \
    # install video I/O packages \
    && apt-get -qy install libavcodec-dev \
	libavformat-dev \
	libswscale-dev \
	libv4l-dev \
	v41-utils \
    libxvidcore-dev \
    libx264-dev \
    # install GTK development library \
    && apt-get -qy install libgtk2.0-dev \
    && modprobe bcm2835-v412 \
    # install optimize dependencies \
    && apt-get -qy install libatlas-base-dev \
    gfortran \
    libgtkglext1 \
    libgtkglext1-dev \
    # install python \
    && apt-get -qy install python python3 \
    python3-pip python2.7-dev python2-numpy \
    python3-dev python3-numpy \
    # install opencv \
    && wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.4.0.zip \
    && unzip opencv.zip \
    && wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.4.0.zip \
    unzip opencv_contrib.zip \
    && cd ./opencv-3.4.0/ \
    && mkdir build \
    && cd build \
    && cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D BUILD_opencv_java=OFF \
	-D BUILD_opencv_python2=ON \
	-D BUILD_opencv_python3=ON \
	-D PYTHON_DEFAULT_EXECUTABLE=$(which python3) \
	-D BUILD_WITH_DEBUG_INFO=OFF \
	-D BUILD_DOCS=OFF \
	-D BUILD_EXAMPLES=ON \
	-D BUILD_TESTS=OFF \
	-D BUILD_opencv_ts=OFF \
	-D BUILD_PERF_TESTS=OFF \
	-D INSTALL_C_EXAMPLES=ON \
	-D INSTALL_PYTHON_EXAMPLES=ON \
	-D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-3.4.0/modules \
	-D ENABLE_NEON=0N \
	-D WITH_LIBV4L=ON \
	-D WITH_OPENGL=ON ../ \
    && make -j4 \
    && make install \
    && ldconfig \
    && cd .. \
    && cd .. \
    && rm -rf opencv.zip opencv_contrib.zip \
    && rm -rf opencv-3.4.0/ opencv_contrib-3.4.0 \
    # install gstreamer \
    && apt-get -qy install git vim git \
    build-essential dpkg-dev flex bison \
    autotools-dev automake \
    autopoint libtool gtk-doc-tools libgstreamer1.0-dev \
    libxv-dev libasound2-dev \
    libtheora-dev libogg-dev libvorbis-dev \
    libbz2-dev libv4l-dev libvpx-dev libjack-jackd2-dev \
    libsoup2.4-dev libpulse-dev \
    faad libfaad-dev libgl1-mesa-dev libgles2-mesa-dev \
    libx264-dev libmad0-dev \
    && apt-get -qy install vdpau-va-driver vdpauinfo libvdpau-dev \
    libva-dev vainfo \
    && apt-get -qy install gstreamer1.0-tools \
	                    gstreamer1.0-plugins-base \
						gstreamer1.0-plugins-good \
						gstreamer1.0-libav \
						gstreamer1.0-omx-rpi \
						gstreamer1.0-plugins-bad \
						gstreamer1.0-plugins-ugly \
						gstreamer1.0-rtsp \
    && gst_version=$(gst-inspect-1.0 --gst-version | grep -o -E "[0-9]{1,2}.[0-9]{1,2}.[0-9]{1,2}") \
    && wget https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-$gst_version.tar.xz \
    && tar xf gstreamer-$gst_version.tar.xz \
    && wget https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-$gst_version.tar.xz \
    && tar xf gst-plugins-base-$gst_version.tar.xz \
    && wget https://gstreamer.freedesktop.org/src/gst-rtsp-server/gst-rtsp-server-$gst_version.tar.xz \
    && tar xf gst-rtsp-server-$gst_version.tar.xz \
    && cd gstreamer-$gst_version \
    && ./configure && make && sudo make install && sudo ldconfig \
    && cd ../gst-plugins-base-$gst_version \
    && ./configure && make && sudo make install && sudo ldconfig \
    && cd ../gst-rtsp-server-$gst_version \
    && ./configure && make && sudo make install && sudo ldconfig \
    && rm -rf gstreamer-$gst_version gst-plugins-base-$gst_version \
			gst-rtsp-server-$gst_version \
			gstreamer-$gst_version.tar.xz \
			gst-plugins-base-$gst_version.tar.xz \
			gst-rtsp-server-$gst_version.tar.xz \
    # install restream-camera app \
    && git clone https://github.com/cvkien1996/gstreamer-camera-restream.git \
    && cd gstreamer-camera-restream \
    && make \
    && cp main /usr/bin/restream-camera \
    && cd .. \
    && git clone https://hieunvce@bitbucket.org/hieunvce/software-deployment.git \
    && cd ./software-deployment/RestreamCamera/ \
    && yes | sudo cp ./services/restream-camera.service /etc/systemd/system/ \
    && systemctl enable restream-camera.service \
    && systemctl daemon-reload

CMD [ "/bin/bash" ]